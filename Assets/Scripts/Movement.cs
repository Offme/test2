﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    private Rigidbody2D rb;
    [SerializeField] 
    private Vector2 launchPos;
    [SerializeField]
    private Vector2 startPos;
    [SerializeField]
    private Vector2 endPos;
    [SerializeField]
    private Vector2 direction;
    [SerializeField]
    private bool isDir;
    
    public GameManager GM;

    public GameObject col;
    public GameObject wall1;
    public GameObject wall2;
    public GameObject wall3;
    
    public float force;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        launchPos = transform.position;
    }

    private void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            Debug.Log("touch");
            switch (touch.phase)
            {
                case TouchPhase.Began:
                {
                    col.SetActive(true);
                    wall1.SetActive(true);
                    wall2.SetActive(true);
                    wall3.SetActive(true);
                    
                    rb.velocity = Vector2.zero;
                    Vector2 touchPos = Camera.main.ScreenToWorldPoint(touch.position);
                    transform.position = touchPos;
                    startPos = touch.position;
                    isDir = false;
                    Debug.Log("startPos " + startPos);
                    break;
                }

                case TouchPhase.Moved:
                {
                    Vector2 touchPos = Camera.main.ScreenToWorldPoint(touch.position);
                    rb.velocity = (touchPos - new Vector2(transform.position.x, transform.position.y)) * 5;
                    if (touchPos == new Vector2(transform.position.x, transform.position.y))
                    {
                        rb.velocity = Vector2.zero;
                    }
                    break;
                }

                case TouchPhase.Ended:
                {
                    col.SetActive(false);
                    wall1.SetActive(false);
                    wall2.SetActive(false);
                    wall3.SetActive(false);
                    
                    endPos = touch.position;
                    direction = endPos - startPos;
                    isDir = true;
                    Debug.Log("end " + isDir);
                    break;
                }
            }
        }

        if (isDir)
        {
            rb.AddForce(direction * force, ForceMode2D.Impulse);
            isDir = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Wall lr"))
        {
            rb.velocity = new Vector2(-rb.velocity.x, rb.velocity.y);
        }
        
        if (other.gameObject.CompareTag("Wall ud"))
        {
            rb.velocity = new Vector2(rb.velocity.x, -rb.velocity.y);
        }

        if (other.gameObject.CompareTag("Shape"))
        {
            GameObject[] Shapes = GameObject.FindGameObjectsWithTag("Shape");
            
            for (int i = 0; i < Shapes.Length; i++)
            {
                Destroy(Shapes[i]);
            }

            GameObject Won = GameObject.FindGameObjectWithTag("Win");
            
            Destroy(Won);
            
            GM.Spawn();
            
            transform.position = launchPos;
            rb.velocity = new Vector2(0, 0);
        }
        
        if (other.gameObject.CompareTag("Win"))
        {
            transform.position = launchPos;
            
            rb.velocity = Vector2.zero;
            
            GameObject[] Shapes = GameObject.FindGameObjectsWithTag("Shape");
            
            for (int i = 0; i < Shapes.Length; i++)
            {
                Destroy(Shapes[i]);
            }

            GameObject Won = GameObject.FindGameObjectWithTag("Win");
            
            Destroy(Won);
            
            GM.NextLevel();
            GM.Spawn();
        }
    }
}
