﻿using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public Vector2 RandomRange;
    public GameObject Shape;

    public int minCount;
    public int maxCount;
    public int count;

    public int level;
    public Text levelNow;

    public GameObject[] Wins;
    public Transform Canv;
    
    private void Start()
    {
        Spawn();
    }

    public void NextLevel()
    {
        Analytics.CustomEvent("level_complete", new Dictionary<string, object>
        {
            {"level_id", level}
        });
        level++;
        levelNow.text = "" + level;
    }
    
    public void Spawn()
    {
        int r = Random.Range(0, Wins.Length);
        GameObject clonee = Instantiate(Wins[r], Vector3.zero, quaternion.identity, Canv);

        count = Random.Range(minCount, maxCount);

        for (int i = 0; i < count; i++)
        {
            RandomRange = new Vector2(Random.Range(-1.5f, 1.5f), Random.Range(-1, 4));
            GameObject clone = Instantiate(Shape, RandomRange, quaternion.identity, transform.parent);
        }
    }

    public void Destroyer()
    {
        for (int i = 0; i < count; i++)
        {
            Destroy(GameObject.FindGameObjectWithTag("Shape"));
        }
        Destroy(GameObject.FindGameObjectWithTag("Win"));
    }
}
